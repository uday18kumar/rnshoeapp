import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import {Header,Left,Right,Body,Icon} from 'native-base'
import Styles from '../../containers/common/style'
import PropTypes from 'prop-types'
import { responsiveWidth } from 'react-native-responsive-dimensions';

const propTypes = {
    headerTitle:PropTypes.string,
    leftIconName:PropTypes.string,
    leftIconType:PropTypes.string,
    leftIconFn:PropTypes.func,
    rightIconName1:PropTypes.string,
    rightIconFn1:PropTypes.func,
    rightIconType1:PropTypes.string,
    rightIconName2:PropTypes.string,
    rightIconFn2:PropTypes.func,
    rightIconType2:PropTypes.string,
    rightText:PropTypes.string,
    rightTextFn:PropTypes.func
}

const defaultProps = {
    headerTitle:'',
    leftIconName:'',
    rightIconName1:'',
    rightIconName2:'',
    leftIconType:'',
    rightIconType1:'',
    rightIconType2:'',
    leftIconFn:()=>{},
    rightIconFn1:()=>{},
    rightIconFn2:()=>{},
    rightText:'',
    rightTextFn:()=>{}

}

class ShoeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Header style={{backgroundColor:'white'}} >
            <Left style={{flex:.2}}>
                <TouchableOpacity onPress={()=>this.props.leftIconFn()}>
                    <Icon name={this.props.leftIconName} 
                    type={this.props.leftIconType}
                    style={Styles.fontSize_2}
                    />
                </TouchableOpacity>
            </Left>

            <Body style={{flex:.6,alignItems:'center'}}>
                <Text style={[Styles.fontSize_2,Styles.fontBold,{color:'black'}]}>
                    {this.props.headerTitle}</Text>
            </Body>

            <Right style={{flex:.2}}>
                <TouchableOpacity onPress={()=>this.props.rightTextFn()}>
                <Text style={[Styles.fontSize_1_5,
                                {color:'black'}]}>{this.props.rightText}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.props.rightIconFn1()}
                    style={{marginRight:responsiveWidth(2)}}
                >
                    <Icon name={this.props.rightIconName1} 
                    type={this.props.rightIconType1}
                    style={Styles.fontSize_2}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.props.rightIconFn2()}>
                    <Icon name={this.props.rightIconName2} 
                    type={this.props.rightIconType2}
                    style={Styles.fontSize_2}
                    />
                </TouchableOpacity>
            </Right>
        </Header>
    );
  }
}

ShoeHeader.defaultProps = defaultProps;
ShoeHeader.propTypes = propTypes;

export default ShoeHeader;
