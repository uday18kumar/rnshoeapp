import React, { Component } from 'react';
import { View, Text } from 'react-native';

import StarRating from 'react-native-star-rating';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import PropTypes from 'prop-types';

const propTypes = {
    full_StarColor:PropTypes.string,
    empty_StarColor:PropTypes.string,
    selectedStarFn:PropTypes.func,
    fnDisabled:PropTypes.bool,
    rating:PropTypes.number
  };
  
  const defaultProps = {
    full_StarColor:'#FDB24A',
    empty_StarColor:'#D7D7D7',
    selectedStarFn:()=>{},
    fnDisabled:false,
    rating:0
  };

class StarRatingView extends Component {
  constructor(props) {
    super(props);
    this.state = {
        starCount:3.5
    };
  }
  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
 
  render() {
    return (
        <StarRating
        disabled={this.props.fnDisabled}
        emptyStar={'ios-star'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={5}
        rating={this.props.rating}
        selectedStar={(rating) => this.props.selectedStarFn(rating)}
        fullStarColor={this.props.full_StarColor}
        emptyStarColor={this.props.empty_StarColor}
        starSize={responsiveFontSize(2.5)}
      />
    );
  }
}

StarRatingView.defaultProps = defaultProps;
StarRatingView.propTypes = propTypes;

export default StarRatingView;

