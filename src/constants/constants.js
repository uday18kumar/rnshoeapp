export const BrandsArray = [
    {
        name:'Nike'
    },
    {
        name:'Adidas'
    },
    {
        name:'Reebok'
    }
];


export const usSizes = ['6','6.5','7','7.5','8','8.5','9','9.5','10','10.5','11','11.5','12','13','14'];

export const ukSizes = ['5.5','6','6.5','7','7.5','8','8.5','9','9.5','10','10.5','11','11.5','12.5','13.5','14.5'];

export const euSizes = ['39','39','40','40-41','41','41-42','42','42-43','43-44','43-44.5','44-45','45-46','46-47','47-48','48-49'];

export const ColorsArray = ['#fff','#000','#B6B6B6','#E0E0E0','#04ADEF','#EC2327','#15FFFF','#FF0900'];

export const ShoesArray = [
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image1.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image2.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image3.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image1.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image2.jpg')
    },
    {
      text:"AIR JORDAN1 RETRO HIGH OG 'ORIGIN STORY'",
      img:require('../../assets/images/shoe_image3.jpg')
    },
    ];
