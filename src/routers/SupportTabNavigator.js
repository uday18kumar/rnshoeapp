import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {createMaterialTopTabNavigator,createAppContainer} from 'react-navigation'
import OpenTab from '../containers/protectedScenes/tabs/support/openTab'
import ResolvedTab from '../containers/protectedScenes/tabs/support/resolvedTab'


const SupportTabNavigator = createMaterialTopTabNavigator(
    {
        Open:{screen:OpenTab,
            navigationOptions:({
              tabBarOptions:{
                showIcon:false,
                showLabel:true,
                activeTintColor:'#A37AB5',
                inactiveTintColor:'black',
                indicatorStyle: {backgroundColor:'#A37AB5'},
                style:{
                    backgroundColor:'white',
                 
                },
                labelStyle:{
                        fontWeight: 'bold',
                },
                allowFontScaling:false
              },  
            })
        },
        Resolved:{screen:ResolvedTab,
            navigationOptions:({
                tabBarOptions:{
                  showIcon:false,
                  showLabel:true,
                  activeTintColor:'#A37AB5',
                  inactiveTintColor:'black',
                  indicatorStyle: {backgroundColor:'#A37AB5'},
                  style:{
                    backgroundColor:'white'
                  },
                  labelStyle:{
                    fontWeight: 'bold',
                    },
                  allowFontScaling:false
                },  
              })
        }

    }
)

export default createAppContainer(SupportTabNavigator);
