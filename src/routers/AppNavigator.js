import React from 'react';
import { createAppContainer,createStackNavigator, createSwitchNavigator } from 'react-navigation';

// Import routes - Importing main scenes directly for now, This should import StackNavigation Components Later
import SplashScreen from '../containers/splash';
import SplashScreen2 from '../containers/splash/splash2';
import Login from '../containers/login';
import ForgetPassword from '../containers/forgetPassword';
import SignUp from '../containers/signup';
import AddShoeSize from '../containers/addShoeSize';
import Settings from '../containers/protectedScenes/settings'
import EditProfile from '../containers/protectedScenes/settings/editProfile'
import ChangePassword from '../containers/protectedScenes/settings/changePass'
import ManageCredCards from '../containers/protectedScenes/settings/manageCrCards'
import ManageAddress from '../containers/protectedScenes/settings/manageAddress'
import TabNavigation from './TabNavigator';
import AddCreditCard from '../containers/protectedScenes/settings/addCreditCard'
import NotificationSettings from '../containers/protectedScenes/settings/notifications'
import Filter from '../containers/protectedScenes/filter'
import ProductDesc from '../containers/protectedScenes/productDesc'
import AddMoreSizes from '../containers/protectedScenes/addMoreSizes'

// Route Configs
const AppNavigator = createStackNavigator(
  {
    SplashScreen:SplashScreen,
    SplashScreen2:SplashScreen2,
    Login:Login,
    ForgetPassword:ForgetPassword,
    SignUp:SignUp,
    AddShoeSize:AddShoeSize,
    Settings:Settings,
    EditProfile:EditProfile,
    ChangePassword:ChangePassword,
    ManageCredCards:ManageCredCards,
    ManageAddress:ManageAddress,
    TabNavigation:{screen:TabNavigation,
                    navigationOptions:{
                      header:null
                    }},
    AddCreditCard:AddCreditCard,
    NotificationSettings:NotificationSettings,
    Filter:Filter,
    ProductDesc:ProductDesc,
    AddMoreSizes:AddMoreSizes
  },
  {
    initialRouteName: 'SplashScreen',
    /* The header config from HomeScreen is now here */
    headerBackTitleVisible:false,
    headerMode:'screen',
    defaultNavigationOptions: {
    },
  }
);

// Create Tab Navigator

const MainStackNavigation = createAppContainer(AppNavigator);

export default MainStackNavigation;
