import React from 'react';
import { Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import ProductTab from '../containers/protectedScenes/tabs/productDashboard'
import SearchTab from '../containers/protectedScenes/tabs/searchTab'
import SupportTab from '../containers/protectedScenes/tabs/support'
import ProfileTab from '../containers/protectedScenes/tabs/profile'

import { Icon } from "native-base";
const TabNavigator = createBottomTabNavigator(
    {
      Product:{screen:ProductTab,
        navigationOptions:({  
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
          // You can return any component that you like here!
            return  <Icon name='shoe-formal' type={'MaterialCommunityIcons'}
                   style={{color:tintColor}}/>;
            },
          tabBarOptions:{
            showIcon:true,
            showLabel:true,
            activeTintColor:'#A37AB5',
            inactiveTintColor:'black',
            indicatorStyle: {backgroundColor:'#ffffff'},
            allowFontScaling:false
          },  
        })
    },
      Search:{screen:SearchTab,
        navigationOptions:({  
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
          // You can return any component that you like here!
            return  <Icon name='search'
                   style={{color:tintColor}}/>;
            },
          tabBarOptions:{
            showIcon:true,
            showLabel:true,
            activeTintColor:'#A37AB5',
            inactiveTintColor:'black',
            indicatorStyle: {backgroundColor:'#ffffff'},
            allowFontScaling:false
          },  
        })
    },
      Support:{screen:SupportTab,
        navigationOptions:({  
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
          // You can return any component that you like here!
            return  <Icon name='message1' type={'AntDesign'}
                   style={{color:tintColor}}/>;
            },
          tabBarOptions:{
            showIcon:true,
            showLabel:true,
            activeTintColor:'#A37AB5',
            inactiveTintColor:'black',
            indicatorStyle: {backgroundColor:'#ffffff'},
            allowFontScaling:false
          },  
        })
    },
      Profile:{screen:ProfileTab,
        navigationOptions:({  
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
          // You can return any component that you like here!
            return  <Icon name='person' 
                   style={{color:tintColor}}/>;
            },
          tabBarOptions:{
            showIcon:true,
            showLabel:true,
            activeTintColor:'#A37AB5',
            inactiveTintColor:'black',
            indicatorStyle: {backgroundColor:'#ffffff'},
            allowFontScaling:false
          },  
        })
    },
 
    },
    {
      initialRouteName:'Product',
      backBehavior:'initialRoute',
    }
);

export default createAppContainer(TabNavigator)

