import React, { Component } from 'react';
import { View, Text } from 'react-native';
import AppNavigator from './routers/AppNavigator';


class Main extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true;
    this.state = {
    };
  }

  render() {
    return (
        <AppNavigator />
    );
  }
}

export default Main;
