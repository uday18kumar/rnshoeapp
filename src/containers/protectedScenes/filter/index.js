import React, { Component } from 'react';
import { View, Text, TouchableOpacity,ScrollView ,TouchableWithoutFeedback,StyleSheet} from 'react-native';
import { Container, Content, Icon, Left, Right, Input, ListItem, CheckBox, Item } from 'native-base';
import ShoeHeader from '../../../components/ShoeHeader';
import Styles from '../../common/style';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';

import {BrandsArray,
        ukSizes,
        usSizes,
        euSizes,
        ColorsArray} from '../../../constants/constants'

import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['name'];
       


class Filter extends Component {

    static navigationOptions = ({
        header:null
    });

  constructor(props) {
    super(props);
    this.state = {
        gender:'Women',
        shoeCode:'US',
        shoeSizes:[],
        sizeToggle:false,
        brandToggle:false,
        selectedBrands:[],
        conditionToggle:false,
        cond:'New',
        categoryToggle:false,
        category:'Basketball',
        colorToggle:false,
        colors:[],
        searchTerm:''
    };
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }
  
  render() {
    const filteredBrands = BrandsArray.filter(createFilter(this.state.searchTerm,KEYS_TO_FILTERS))
    return (
        <Container>
            <ShoeHeader 
                headerTitle={'FILTERS'}
                leftIconName={'left'}
                leftIconType={'AntDesign'}
                leftIconFn = {()=>this.props.navigation.navigate('Product')}
                rightText={'Clear All'}
                rightTextFn={()=>this.clearAllFilters()}
            />
            <Content>
                <View style={[Styles.height_7,Styles.justifyCenter]}>
                    <TouchableWithoutFeedback onPress={()=>this.setState({sizeToggle : (this.state.sizeToggle) ? false:true})}>
                    <View style={{flexDirection:'row'}}>
                        
                        <Left>
                        <Text style={[Styles.fontSize_3,
                                    Styles.fontBold,
                                    {paddingLeft:responsiveWidth(3),
                                    color:'#000'}]}>SIZE</Text>
                        </Left>
                        <Right>
                        {(this.state.sizeToggle) ? (
                            <Icon name={'down'} type={'AntDesign'} 
                                style={[Styles.fontSize_3]} />
                        ):(
                            <Icon name={'right'} type={'AntDesign'}
                                 style={[Styles.fontSize_3]} />
                        )}
                        </Right>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
                {(this.state.sizeToggle) && 
                <View>
                <View style={[{flexDirection:'row',
                                paddingTop:responsiveHeight(2),
                                justifyContent:'space-around'},
                                Styles.width_60]}>
                    <TouchableOpacity style={[{backgroundColor:(this.state.gender== 'Men')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                            onPress={()=>this.setState({gender:'Men'})}
                                        >
                        <Text style={{color:'#fff'}}>Men</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[{backgroundColor:(this.state.gender== 'Women')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                                onPress={()=>this.setState({gender:'Women'})}
                                        >
                        <Text style={{color:'#fff'}}>Women</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[{backgroundColor:(this.state.gender== 'Youth')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                                onPress={()=>this.setState({gender:'Youth'})}
                                        >
                        <Text style={{color:'#fff'}}>Youth</Text>
                    </TouchableOpacity>

                </View>

                <View style={[{flexDirection:'row',
                                paddingTop: responsiveHeight(2),
                                justifyContent:'space-around'},Styles.width_50]}>
                    <TouchableOpacity style={[{backgroundColor:(this.state.shoeCode== 'UK')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                    Styles.borderRadius_1]}
                            onPress={()=>this.setState({shoeCode:'UK'})}
                                        >
                        <Text style={{color:'#fff'}}>UK</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[{backgroundColor:(this.state.shoeCode== 'US')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                                onPress={()=>this.setState({shoeCode:'US'})}
                                        >
                        <Text style={{color:'#fff'}}>US</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[{backgroundColor:(this.state.shoeCode== 'EU')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                    Styles.borderRadius_1]}
                                onPress={()=>this.setState({shoeCode:'EU'})}
                                        >
                        <Text style={{color:'#fff'}}>EU</Text>
                    </TouchableOpacity>

                </View>

                <ScrollView horizontal={true}
                            contentContainerStyle={[
                                            Styles.justifyCenter,
                                            Styles.alignCenter,
                                            {height:responsiveHeight(15),
                                            marginTop:responsiveHeight(4)},
                                            ]}  >
                            {(this.state.shoeCode == 'UK') && 
                                 this.renderSizes()
                            }
                            {(this.state.shoeCode == 'US') && 
                                 this.renderSizes()
                            }
                            {(this.state.shoeCode == 'EU') && 
                                 this.renderSizes()
                            }
                </ScrollView> 
                </View> 
                }


                <View style={[Styles.height_7,Styles.justifyCenter]}>
                    <TouchableWithoutFeedback onPress={()=>this.setState({brandToggle : (this.state.brandToggle) ? false:true})}>
                    <View style={{flexDirection:'row'}}>
                        
                        <Left>
                        <Text style={[Styles.fontSize_3,
                                    Styles.fontBold,
                                    {paddingLeft:responsiveWidth(3),
                                    color:'#000'}]}>BRAND</Text>
                        </Left>
                        <Right>
                        {(this.state.brandToggle) ? (
                            <Icon name={'down'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        ):(
                            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        )}
                        </Right>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
                    {(this.state.brandToggle) && 
                        <View >
                            <SearchInput 
                                onChangeText={(term) => { this.searchUpdated(term) }} 
                                style={styles.searchInput}
                                placeholder="Search by name & SKU"
                            />
                          {this.renderBrands(filteredBrands)}
                           
                        </View>
                    }

                <View style={[Styles.height_7,Styles.justifyCenter]}>
                    <TouchableWithoutFeedback onPress={()=>this.setState({conditionToggle : (this.state.conditionToggle) ? false:true})}>
                    <View style={{flexDirection:'row'}}>
                        
                        <Left>
                        <Text style={[Styles.fontSize_3,
                                    Styles.fontBold,
                                    {paddingLeft:responsiveWidth(3),
                                    color:'#000'}]}>CONDITION</Text>
                        </Left>
                        <Right>
                        {(this.state.conditionToggle) ? (
                            <Icon name={'down'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        ):(
                            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        )}
                        </Right>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
                    {(this.state.conditionToggle) && 
                    <View>
                        <View style={[{flexDirection:'row',
                                paddingTop:responsiveHeight(2),
                                justifyContent:'space-around'},
                                Styles.width_50]}>
                    <TouchableOpacity style={[{backgroundColor:(this.state.cond== 'New')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                            onPress={()=>this.setState({cond:'New'})}
                                        >
                        <Text style={{color:'#fff'}}>New</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[{backgroundColor:(this.state.cond== 'Used')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                                onPress={()=>this.setState({cond:'Used'})}
                                        >
                        <Text style={{color:'#fff'}}>Used</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                    }
                            
                    <View style={[Styles.height_7,Styles.justifyCenter]}>
                    <TouchableWithoutFeedback onPress={()=>this.setState({categoryToggle : (this.state.categoryToggle) ? false:true})}>
                    <View style={{flexDirection:'row'}}>
                        
                        <Left>
                        <Text style={[Styles.fontSize_3,
                                    Styles.fontBold,
                                    {paddingLeft:responsiveWidth(3),
                                    color:'#000'}]}>CATEGORY</Text>
                        </Left>
                        <Right>
                        {(this.state.categoryToggle) ? (
                            <Icon name={'down'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        ):(
                            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        )}
                        </Right>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
                {(this.state.categoryToggle) && 
                    <View>
                        <View style={[{flexDirection:'row',
                                paddingTop:responsiveHeight(2),
                                justifyContent:'space-around'},
                                Styles.width_80]}>
                    <TouchableOpacity style={[{backgroundColor:(this.state.category== 'Basketball')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                            onPress={()=>this.setState({cond:'Basketball'})}
                                        >
                        <Text style={{color:'#fff'}}>Basketball</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={[{backgroundColor:(this.state.category== 'Running')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                                onPress={()=>this.setState({category:'Running'})}
                                        >
                        <Text style={{color:'#fff'}}>Running</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[{backgroundColor:(this.state.category== 'Skateball')?'#000':'#A0A0A0',
                                        paddingHorizontal:responsiveWidth(4),
                                        paddingVertical:responsiveHeight(1)},
                                        Styles.borderRadius_1]}
                                onPress={()=>this.setState({category:'Skateball'})}
                                        >
                        <Text style={{color:'#fff'}}>Skateball</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                    }

                <View style={[Styles.height_7,Styles.justifyCenter]}>
                    <TouchableWithoutFeedback onPress={()=>this.setState({colorToggle : (this.state.colorToggle) ? false:true})}>
                    <View style={{flexDirection:'row'}}>
                        
                        <Left>
                        <Text style={[Styles.fontSize_3,
                                    Styles.fontBold,
                                    {paddingLeft:responsiveWidth(3),
                                    color:'#000'}]}>COLOR</Text>
                        </Left>
                        <Right>
                        {(this.state.colorToggle) ? (
                            <Icon name={'down'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        ):(
                            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_3]} />
                        )}
                        </Right>
                    </View>
                    </TouchableWithoutFeedback>
                </View>

                    {(this.state.colorToggle) && 
                        <View style={[{flexDirection:'row',
                                        flexWrap: 'wrap',
                                        paddingVertical:responsiveHeight(2)
                                         },
                                        Styles.width_70]}>
                            {this.renderColors()}
                        </View>

                    }

                <View style={[{flex:1},
                                Styles.alignCenter,
                                Styles.height_10,
                                Styles.justifyCenter]}>
                    <TouchableOpacity style={[{paddingVertical:responsiveHeight(1.5),
                                                backgroundColor:'black',
                                                borderRadius:responsiveWidth(2),
                                                elevation:10},
                                                Styles.width_50,
                                                Styles.alignCenter]}
                                                >
                                <Text style={[{color:'white'},Styles.fontSize_2]}>APPLY FILTER</Text>
                    </TouchableOpacity>
                </View>
            </Content>
        </Container>
      
    );
  }

  renderBrands(object){
      return object.map((item)=>{
          return(
                <ListItem onPress={()=>this.addBrand(item.name)}>
                <View style={[{flex:.1},Styles.justifyCenter,Styles.alignCenter]}>
                <Icon name='shoe-formal'
                    type={'MaterialCommunityIcons'}
                    />
                </View>
                <View style={[{flex:.9,paddingLeft:responsiveWidth(3)},
                                Styles.justifyCenter]}>
                <Text style={[Styles.fontBold,
                            Styles.fontSize_2,
                            {color:'#000'}]}>{item.name}</Text>
                </View>
                {(this.state.selectedBrands.includes(item.name)) ? (
                    <CheckBox 
                    color={'#000'} 
                    checked={true}
                    onPress={()=>this.addBrand(item.name)}
                    />
                ):(
                <CheckBox 
                color={'#000'} 
                checked={false}
                onPress={()=>this.addBrand(item.name)}
                />
                )}
             </ListItem>
          )
      })
  }


  renderSizes(){
    if(this.state.shoeCode == 'UK')
    {
      return ukSizes.map((item)=>{
          return(
                <TouchableOpacity style={{height:responsiveHeight(10)}}
                  onPress={()=>this.addShoeSizetoArray(item)}
                >
                    <View style={{backgroundColor:(this.state.shoeSizes.includes(item))?'black':'white',
                        width:responsiveWidth(14),
                        height:responsiveHeight(8),
                        marginHorizontal:responsiveWidth(2),
                        alignItems:'center',
                        justifyContent:'center',
                        elevation:10,
                        borderRadius: responsiveWidth(1),
                        }} > 
                        <Text style={[Styles.fontSize_2,
                                        Styles.fontBold,
                                        {color:(this.state.shoeSizes.includes(item))?'white':'black'}]}>
                            {item}
                        </Text>           
                    </View>
                </TouchableOpacity>
          );
      });
    }
    else if(this.state.shoeCode == 'US'){
      return usSizes.map((item)=>{
          return(

            <TouchableOpacity style={{height:responsiveHeight(10)}}
                      onPress={()=>this.addShoeSizetoArray(item)}
            >
                <View style={{backgroundColor:(this.state.shoeSizes.includes(item))?'black':'white',
                    width:responsiveWidth(14),
                    height:responsiveHeight(8),
                    marginHorizontal:responsiveWidth(2),
                    alignItems:'center',
                    justifyContent:'center',
                    elevation:10,
                    borderRadius: responsiveWidth(1),
                    }} > 
                    <Text style={[Styles.fontSize_2,
                                    Styles.fontBold,
                                    {color:(this.state.shoeSizes.includes(item))?'white':'black'}]}>
                        {item}
                    </Text>           
                </View>
                </TouchableOpacity>
          );
      });
    }
    else if(this.state.shoeCode == 'EU'){
      return euSizes.map((item)=>{
          return(
            <TouchableOpacity style={{height:responsiveHeight(10)}}
            onPress={()=>this.addShoeSizetoArray(item)}
            >
                <View style={{backgroundColor:(this.state.shoeSizes.includes(item))?'black':'white',
                    width:responsiveWidth(14),
                    height:responsiveHeight(8),
                    marginHorizontal:responsiveWidth(2),
                    alignItems:'center',
                    justifyContent:'center',
                    elevation:10,
                    borderRadius: responsiveWidth(1),
                    }} > 
                    <Text style={[Styles.fontSize_1_8,
                                    Styles.fontBold,
                                    {color:(this.state.shoeSizes.includes(item))?'white':'black'}]}>
                        {item}
                    </Text>           
                </View>
                </TouchableOpacity>
          );
        });
        }  
    }

    renderColors(){
        return ColorsArray.map((item)=>{
            return(
                <TouchableWithoutFeedback onPress={()=>this.addColors(item)}>
                <View style={[{height:responsiveHeight(6),
                    width:responsiveWidth(11),
                    backgroundColor:item,
                    elevation:10,
                    margin:responsiveWidth(2),
                    borderRadius:responsiveWidth(50)},
                        Styles.alignCenter,Styles.justifyCenter]}>
                    {(this.state.colors.includes(item)) && 
                    (<Icon 
                        name={'check'}
                        type={'MaterialIcons'}
                        style={[Styles.fontSize_3,{color:(item == '#fff')? '#000' : 'white'}]}
                    />)}
                </View>
                </TouchableWithoutFeedback>
            );
        })
    }
        addShoeSizetoArray(value)
        {
            let shoeArray = this.state.shoeSizes;
            if(!shoeArray.includes(value))
            {
                shoeArray.push(value);
            }
            else
            {
                shoeArray.splice(shoeArray.indexOf(value),1);
            }
            this.setState({shoeSizes:shoeArray});
        }

        addBrand(name){
            let Bname = name;
            let Brands = this.state.selectedBrands;
            if(!Brands.includes(Bname)){
                Brands.push(Bname);
            }
            else{
                Brands.splice(Brands.indexOf(Bname),1);
            }
            this.setState({selectedBrands:Brands})
        }

        addColors(item){
                let selColor = item;
                let SelectedColors = this.state.colors;
                if(!SelectedColors.includes(selColor)){
                    SelectedColors.push(item)
                }
                else{
                    SelectedColors.splice(SelectedColors.indexOf(item),1)
                }
            this.setState({colors:SelectedColors})
        }
        
        clearAllFilters(){
            this.setState({
                selectedBrands:[],
                colors:[],
                category:'',
                cond:'',
                gender:'',
                shoeSizes:[]
            })
        }
}


const styles = StyleSheet.create({
    searchInput:{
      padding: 10,
      borderColor: '#CCC',
      borderWidth: 1,
      textAlign:'center',
      alignSelf: 'center',
      width:responsiveWidth(90),
      borderRadius: responsiveWidth(2),
      marginVertical: responsiveHeight(2)
    }
  })

export default Filter;
