import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import {Container,Content, Input,Footer} from 'native-base'
import Styles from '../../../common/style'
import ShoeHeader from '../../../../components/ShoeHeader'
import {responsiveHeight,responsiveWidth} from 'react-native-responsive-dimensions'

class ChangePass extends Component {
  static navigationOptions = ({
    header:null
  });

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <ShoeHeader 
          headerTitle = {'CHANGE PASSWORD'}
          leftIconName = {'left'}
          leftIconType = {'AntDesign'}
          leftIconFn = {()=>this.props.navigation.navigate('Settings')}
        />
        <Content >
          
          <View style={{flex:1,
                    flexDirection:'column'}}>
            <View style={{flex:.1,
                          flexDirection:'row',
                          alignItems:'center',
                          justifyContent:'center'}}>
                <View style={{flex:.25}}>
                  <Text style={[Styles.fontSize_2]}>Current </Text>
                  <Text style={[Styles.fontSize_2]}>Password </Text>
                </View>
                  
                <Input style={{flex:.65,borderBottomColor:'black',
                                borderBottomWidth:1}} 
                                secureTextEntry
                                />
            </View>

            <View style={{flex:.1,
                        flexDirection:'row',
                        alignItems:'center',
                        justifyContent:'center'}}>
                <View style={{flex:.25}}>
                  <Text style={[Styles.fontSize_2]}>New </Text>
                  <Text style={[Styles.fontSize_2]}>Password </Text>
                </View>
                  
                <Input style={{flex:.65,borderBottomColor:'black',
                                borderBottomWidth:1}}
                                secureTextEntry />
            </View>

            <View style={{flex:.1,
                        flexDirection:'row',
                        alignItems:'center',
                        justifyContent:'center'}}>
                <View style={{flex:.25}}>
                  <Text style={[Styles.fontSize_2]}>Re-enter </Text>
                  <Text style={[Styles.fontSize_2]}>Password </Text>
                </View>
                  
                <Input style={{flex:.65,borderBottomColor:'black',
                                borderBottomWidth:1}}
                                secureTextEntry />
            </View>
          </View>
        </Content>
        <Footer style={[Styles.alignCenter,{backgroundColor:'white',
                                   }]}>
                <TouchableOpacity style={[{paddingVertical:responsiveHeight(1.5),
                                            backgroundColor:'black',
                                            borderRadius:responsiveWidth(2),
                                            elevation:10},
                                            Styles.width_50,
                                            Styles.alignCenter]}
                                            >
                            <Text style={[{color:'white'},Styles.fontSize_2]}>SAVE</Text>
                </TouchableOpacity>
            </Footer>
      </Container>
    );
  }
}

export default ChangePass;
