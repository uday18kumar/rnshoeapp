import React, { Component } from 'react';
import { View, Text,TouchableNativeFeedback } from 'react-native';
import { Container, Content, List, ListItem,Icon} from 'native-base';
import Styles from '../../common/style'
import { responsiveWidth } from 'react-native-responsive-dimensions';
import ShoeHeader from '../../../components/ShoeHeader'


const AccountSettingsArray = [
                                {
                                    name:'Edit Profile',
                                    path:'EditProfile'
                                },
                                {
                                    name:'Change Password',
                                    path:'ChangePassword'
                                },
                                {
                                    name:'Manage Credit Cards',
                                    path:'ManageCredCards'
                                },
                                {
                                    name:'Manage Address',
                                    path:'ManageAddress'
                                },
                            ];

const SettingsArray = [
                        {
                            name:'Notification Settings',
                            path:'NotificationSettings'
                        },
                        {
                            name:'Size Preference Settings',
                            path:'AddShoeSize'
                        }
                        ];

const OthersArray = [
                        {
                            name:'Feedback'
                        },
                        {
                            name:'FAQ'
                        },
                        {
                            name:'Terms & Conditions'
                        }
                    ];
const ConnectedAccountsArray = [
                        {
                            icon:'facebook',
                            name:'Facebook'
                        },
                        {
                            icon:'twitter',
                            name:'Twitter'
                        },
                        {
                            icon:'instagram',
                            name:'Instagram'
                        }
                    ];

class Settings extends Component {
    static navigationOptions = {
        header:null
      };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Container>
            <ShoeHeader 
                headerTitle = {'SETTINGS'}
                leftIconName = {'left'}
                leftIconType = {'AntDesign'}
                leftIconFn = {()=>this.props.navigation.navigate('Profile')}
            />
            <Content contentContainerStyle={{flex:1}}>
                <List>
                    <ListItem itemDivider >
                        <Text style={[Styles.fontBold,
                                    Styles.fontSize_1_8,
                                    {color:'black'}]}>ACCOUNT</Text>
      
                    </ListItem>
                    {this.renderProfile()}

                    <ListItem itemDivider >
                        <Text style={[Styles.fontBold,
                                    Styles.fontSize_1_8,
                                    {color:'black'}]}>SETTINGS</Text>
                    </ListItem>
                   {this.renderSettings()}

                    <ListItem itemDivider >
                        <Text style={[Styles.fontBold,
                                    Styles.fontSize_1_8,
                                    {color:'black'}]}>OTHERS</Text>
                    </ListItem>
                   {this.renderOthers()}
                    <ListItem itemDivider >
                        <Text style={[Styles.fontBold,
                                    Styles.fontSize_1_8,
                                    {color:'black'}]}>CONNECTED ACCOUNTS</Text>
                    </ListItem>
                   {this.renderConnectedAccounts()}
                    
                   
            </List>
            </Content>
        </Container>
    );
  }

  renderProfile(){
      return AccountSettingsArray.map((item)=>{
          return(
            <TouchableNativeFeedback onPress={()=>this.props.navigation.navigate(item.path)}>
            <ListItem style={{flex:1,flexDirection:'row'}}>
            <Text style={[Styles.fontSize_1_8,
                        {color:'black',
                        flex:1}]}>{item.name}</Text>
            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_1_8]} />
            </ListItem> 
            </TouchableNativeFeedback>
          );
      })
  }
  renderSettings(){
      return SettingsArray.map((item)=>{
          return(
            <TouchableNativeFeedback onPress={()=>this.props.navigation.navigate(item.path)}>
                <ListItem style={{flex:1,flexDirection:'row'}}>
            <Text style={[Styles.fontSize_1_8,
                        {color:'black',
                        flex:1}]}>{item.name}</Text>
            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_1_8]} />
            </ListItem> 
            </TouchableNativeFeedback>  
          );
      })
  }
  renderOthers(){
      return OthersArray.map((item)=>{
          return(
            <TouchableNativeFeedback onPress={()=>{alert('hello')}}>
            <ListItem style={{flex:1,flexDirection:'row'}}>
            <Text style={[Styles.fontSize_1_8,
                        {color:'black',
                        flex:1}]}>{item.name}</Text>
            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_1_8]} />
            </ListItem> 
            </TouchableNativeFeedback>  
          );
      })
  }
  renderConnectedAccounts(){
      return ConnectedAccountsArray.map((item)=>{
          return(
            
            <ListItem style={{flex:1,flexDirection:'row'}}>
            <Icon name={item.icon} type={'FontAwesome'}
                 style={[Styles.fontSize_1_8,{paddingRight:responsiveWidth(3)}]}/>
            <Text style={[Styles.fontSize_1_8,
                        {color:'black',
                        flex:1}]}>{item.name}</Text>
            <Icon name={'right'} type={'AntDesign'} style={[Styles.fontSize_1_8]} />
            </ListItem> 
          );
      })
  }
}

export default Settings;
