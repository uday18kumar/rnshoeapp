import React, { Component } from 'react';
import { View, Text , StyleSheet,TouchableOpacity } from 'react-native';
import Styles  from '../../../common/style'
import ShoeHeader from '../../../../components/ShoeHeader'
import { Container, Content,Radio,ListItem,Left,Right,Body,Icon, Item, Input } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';


let AddressArray = [
  {
    address:'Riyana Hew SD-1, Abcd Housing Road, California , US', 
  },
  {
    address:'Riyana Hew SD-1, Abcd Housing Road, California , US', 
  },
  {
    address:'Riyana Hew SD-1, Abcd Housing Road, California , US', 
  },
  ];



class ManageAddress extends Component {
  static navigationOptions = ({
    header:null
  });

  constructor(props) {
    super(props);
    this.state = {
      addNewAddressBool:false
    };
  }

  render() {
    return (
      <Container>
        <ShoeHeader 
          headerTitle = {'SHIPPING TO'}
          leftIconName = {'left'}
          leftIconType = {'AntDesign'}
          leftIconFn = {()=>this.props.navigation.navigate('Settings')}
        />
         <Content >
          <View style={{flex: 1,
                        height:responsiveHeight(10),
                        justifyContent:'center',
                        alignItems: 'flex-end',
                        }}>
                <TouchableOpacity onPress={()=>this.setState({addNewAddressBool:true})}>
                    <Text style={[Styles.fontSize_1_8,
                          {paddingRight:responsiveWidth(4),color:'black'}]}>
                            Add New Shipping Address+</Text>
                </TouchableOpacity>
          </View>
          {/* add address view */}
          {(this.state.addNewAddressBool) && 
          <View style={[{flex:1, 
                              borderColor: '#d6d8db',
                              borderWidth: 1,}]}>
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    Full Name</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'Enter your full name'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>      
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    Phone Number</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'e.g. +9190909090'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>      
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    Country or Region</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'India'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>      
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    Postal Code</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'Enter your postal code'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>      
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    Billing Street Address</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'Enter your billing street address'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>      
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    Apt/Unit</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'Your Apt/Unit'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>      
                <View style={{flex:.1,
                            flexDirection:'row',
                            alignItems:'center'}}>
                      
                      <Text style={[{flex:.4,paddingLeft:responsiveWidth(4)},
                                  Styles.fontSize_1_8]}>
                                    City</Text>
                      
                      <Input style={{flex:.6}} 
                        placeholder={'Enter your city'}
                        placeholderTextColor={'black'}
                        style={[Styles.fontSize_1_8,
                                {color:'black',
                                borderBottomColor:'#d6d8db',
                                borderBottomWidth:1}]}
                      />
                </View>
                <View style={[{flex:.1,paddingVertical:responsiveHeight(4)},Styles.alignCenter]}>
                  <TouchableOpacity style={[{paddingVertical:responsiveHeight(1.5),
                                            backgroundColor:'black',
                                            borderRadius:responsiveWidth(2),
                                            elevation:5},
                                            Styles.width_30,
                                            Styles.alignCenter]}
                                onPress={()=>this.setState({addNewAddressBool:false})}
                                            >
                            <Text style={[{color:'white'},Styles.fontSize_2]}>SAVE</Text>
                </TouchableOpacity>      
                </View>
          </View>
            }

          <View style={{flex:.1,
                      width:responsiveWidth(95),
                      }}>
                      {this.renderAddress()}
          </View>
      </Content>
      </Container>
    );
  }
  renderAddress(){
    return AddressArray.map((item,index)=>{
      return(
        <ListItem style={styles.listItemBorder} 
            onPress={()=>this.setState({selectedCard:index})}>
            <View style={[{flex:.1},Styles.justifyCenter,Styles.alignCenter]} >
        {(this.state.selectedCard == index) ? 
          (
            <Radio selectedColor={'#A37AB5'}
              selected = {true}
            
            />
          ):(
            <Radio selectedColor={'#A37AB5'}
              selected = {false}
            
            />
          )}
            </View>
            <View style={{flex:.5}}></View>
            <View style={[{flex:.4,justifyContent:'flex-end'},Styles.justifyCenter]} >
            <Text style={[Styles.fontSize_1_8,
                          Styles.fontBold,
                          {color:'black',
                          textAlign:'right'}]}>{item.address}</Text>
            </View>
           </ListItem>
      )
    })
  }
}

const styles = StyleSheet.create({
  listItemBorder:{

    marginBottom:responsiveHeight(1),
    paddingRight:0,
    height:responsiveHeight(10)
  }
});


export default ManageAddress;
