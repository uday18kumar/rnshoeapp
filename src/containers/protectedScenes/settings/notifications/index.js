import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container, Content, ListItem,Left,Right } from 'native-base';
import ShoeHeader from '../../../../components/ShoeHeader';
import { Switch } from 'react-native-gesture-handler';
import Styles from '../../../common/style'




class NotificationSettings extends Component {

  static navigationOptions = ({
      header:null
  });
    
  constructor(props) {
    super(props);
    this.state = {
        switchValue:false,
        ListArray :[
            {
                text:'Sneakers size available',
                bool:false
            },
            {
                text:'Your sneakers sold',
                bool:false
            },
            {
                text:'Sneaker shipped for verification',
                bool:false
            },
            {
                text:'Sneakers shipped to you',
                bool:true
            },
            {
                text:'Updates on your sneakers for sale',
                bool:true
            },
            {
                text:'Updates on your offer',
                bool:true
            },
            ]
    };
  }

  render() {
    return (
        <Container>
            <ShoeHeader 
                headerTitle = {'NOTIFICATION SETTINGS'}
                leftIconName={'left'}
                leftIconType = {'AntDesign'}
                leftIconFn = {()=>this.props.navigation.navigate('Settings')}
            />
            <Content>
               {this.renderList()}
            </Content>
        </Container>
    );
  }

  renderList(){
      return this.state.ListArray.map((item,index)=>{
       return( <ListItem>
            <Left>
                <Text style={[{color:'black'},Styles.fontSize_1_8]}>
                    {item.text}
                </Text>
            </Left>
            <Right>
                <Switch  onValueChange = {()=>this.changeSwitchStatus(index)} 
                    trackColor={{true:'#A37AB5',false:'grey'}}
                    thumbColor={'white'}
                    value={item.bool}
                    />
            </Right>
        </ListItem>
        )
      })
  }

  changeSwitchStatus(index){
        let notifyArray = this.state.ListArray;

        let value = notifyArray[index].bool;
   
        if(value == true){
            notifyArray[index].bool = false;
        } 
        else{
            notifyArray[index].bool = true;
        }
        this.setState({ListArray:notifyArray})
  }
}

export default NotificationSettings;
