import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import {Container,Content,Footer,Icon, Input} from 'native-base'
import Styles from '../../../common/style';
import {responsiveHeight,responsiveWidth,responsiveFontSize} from 'react-native-responsive-dimensions';
import ShoeHeader from '../../../../components/ShoeHeader';
class EditProfile extends Component {

  static navigationOptions = {
       header:null
  };

  constructor(props) {
    super(props);
    this.state = {
        hideFooter:false
    };
  }

  render() {
    return (
        <Container>
              <ShoeHeader 
                headerTitle = {'EDIT PROFILE'}
                leftIconName = {'left'}
                leftIconType = {'AntDesign'}
                leftIconFn = {()=>this.props.navigation.navigate('Settings')}
            />
            <Content contentContainerStyle={{flex:1}}>

                <View style={[{flex:.3},Styles.alignCenter,Styles.justifyCenter]}>
                <Icon name={'ios-add-circle-outline'} type={'Ionicons'} 
                                style={{color:'black',
                                fontSize:responsiveFontSize(12)
                                }}
                        />
                        <Text style={[{color:'black'},
                                Styles.fontSize_1_5]}>Change Profile Photo</Text>
                </View>

                <View style={{flex:.7,flexDirection:'column'}}>

                    <View style={{flexDirection:'row'}}>

                            <Text style={[Styles.fontSize_2,
                                        {flex:.25,
                                        paddingLeft: responsiveWidth(4),
                                        alignSelf: 'center'}]}>
                                            Username</Text>
                            <Input 
                                        style={{flex:.75,
                                            borderBottomColor:'#A0A0A0',
                                            borderBottomWidth:1}} />
                    </View>
                    
                    <View style={{flexDirection:'row'}}>

                            <Text style={[Styles.fontSize_2,
                                        {flex:.25,
                                        paddingLeft: responsiveWidth(4),
                                        alignSelf: 'center'}]}>
                                            Fullname</Text>
                            <Input style={{flex:.75,
                                            borderBottomColor:'#A0A0A0',
                                            borderBottomWidth:1}} />
                    </View>

                    <View style={{flexDirection:'row'}}>

                            <Text style={[Styles.fontSize_2,
                                        {flex:.25,
                                        paddingLeft: responsiveWidth(4),
                                        alignSelf: 'center',}]}>
                                            Email</Text>
                            <Input style={{flex:.75,
                                            borderBottomColor:'#A0A0A0',
                                            borderBottomWidth:1}} />
                    </View>

                    <View style={{flexDirection:'row'}}>

                            <Text style={[Styles.fontSize_2,
                                        {flex:.25,
                                        paddingLeft: responsiveWidth(4),
                                        alignSelf: 'center',}]}>
                                            Password</Text>
                            <Input 
                                    secureTextEntry
                                    style={{flex:.75,
                                            borderBottomColor:'#A0A0A0',
                                            borderBottomWidth:1}}
                                    onFocus={()=>this.setState({hideFooter:true})}
                                    onSubmitEditing={()=>this.setState({hideFooter:false})}        
                                    />
                    </View>
                </View>
            </Content>
            {(!this.state.hideFooter) && 
            <Footer style={[Styles.alignCenter,{backgroundColor:'white',
                                   }]}>
                <TouchableOpacity style={[{paddingVertical:responsiveHeight(1.5),
                                            backgroundColor:'black',
                                            borderRadius:responsiveWidth(2),
                                            elevation:10},
                                            Styles.width_50,
                                            Styles.alignCenter]}
                                            >
                            <Text style={[{color:'white'},Styles.fontSize_2]}>SAVE</Text>
                </TouchableOpacity>
            </Footer>}
        </Container>
    );
  }
}

export default EditProfile;