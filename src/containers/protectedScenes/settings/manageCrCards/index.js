import React, { Component } from 'react';
import { View, Text,StyleSheet ,Image,TouchableOpacity} from 'react-native';
import Styles  from '../../../common/style'
import ShoeHeader from '../../../../components/ShoeHeader'
import { Container, Content,Radio,ListItem,Left,Right,Body,Icon, Item } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

let CardsArray = [
                  {
                    cardNum:'XXXX-XXXX-XXXX-1765',
                    cardImg:require('../../../../../assets/images/master.png')
                  },
                  {
                    cardNum:'XXXX-XXXX-XXXX-1766',
                    cardImg:require('../../../../../assets/images/master.png')
                  },
                  {
                    cardNum:'XXXX-XXXX-XXXX-1767',
                    cardImg:require('../../../../../assets/images/master.png')
                  },
                  ]


class ManageCrCards extends Component {
  static navigationOptions = ({
    header:null
  });

  constructor(props) {
    super(props);
    this.state = {
      selectedCard : null
    };
  }

  render() {
    return (
      <Container>
      <ShoeHeader 
        headerTitle = {'CREDIT CARDS'}
        leftIconName = {'left'}
        leftIconType = {'AntDesign'}
        leftIconFn = {()=>this.props.navigation.navigate('Settings')}
      />
      <Content >
          <View style={{flex: 1,
                        height:responsiveHeight(10),
                        justifyContent:'center',
                        alignItems: 'flex-end',
                        }}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('AddCreditCard')}>
                    <Text style={[Styles.fontSize_2,
                          {paddingRight:responsiveWidth(4)}]}>
                            Add Credit/Debit Card+</Text>
                </TouchableOpacity>
          </View>

          <View style={{flex:.1,
                      width:responsiveWidth(95),
                      }}>
                      {this.renderCards()}
          </View>
      </Content>
    </Container>
    );
  }

  delCard(cardNum){
    alert('del cards');
  }

  renderCards(){
    return CardsArray.map((item,index)=>{
      return(
        <ListItem style={styles.listItemBorder} 
            onPress={()=>this.setState({selectedCard:index})}>
            <View style={[{flex:.1},Styles.justifyCenter,Styles.alignCenter]} >
        {(this.state.selectedCard == index) ? 
          (
            <Radio selectedColor={'#A37AB5'}
              selected = {true}
            
            />
          ):(
            <Radio selectedColor={'#A37AB5'}
              selected = {false}
            
            />
          )}
            </View>
            <View style={[{flex:.7,flexDirection:'row'},Styles.alignCenter]} >
          <Image source={item.cardImg}
              resizeMode ={'center'} 
              style={{width:responsiveWidth(10),
                    height:responsiveHeight(5),
                      marginRight:responsiveWidth(4)}}
              />
            <Text>{item.cardNum}</Text>
            </View>
        

            {(this.state.selectedCard == index) &&
            <TouchableOpacity onPress={()=>this.delCard(item.cardNum)}
              style={[{flex:.2,
                      height:responsiveHeight(10),
                      backgroundColor:'#F16164'},
                      Styles.alignCenter,
                      Styles.justifyCenter]}
              > 
              <Icon name={'delete'} style={{color:'white'}} type={'MaterialIcons'}/>
              <Text style={[Styles.fontSize_1_5,{color:'white'}]}>DELETE</Text>
            </TouchableOpacity>
     }

      </ListItem>
      )
    })
  }
}

const styles = StyleSheet.create({
  listItemBorder:{
    borderColor: '#d6d8db',
    borderWidth: 1,
    marginBottom:responsiveHeight(1),
    paddingRight:0,
    height:responsiveHeight(10)
  }
})


export default ManageCrCards;
