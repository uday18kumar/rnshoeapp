import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import { Container, Content, Input ,Footer } from 'native-base';
import ShoeHeader from '../../../../components/ShoeHeader';
import Styles from '../../../common/style'
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
class AddCreditCard extends Component {

  static navigationOptions = ({
      header:null
  }); 

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Container>
            <ShoeHeader 
                headerTitle={'ADD CREDIT CARD'}
                leftIconName = {'left'}
                leftIconType = {'AntDesign'}
                leftIconFn = {()=>this.props.navigation.navigate('ManageCredCards')}
            />
            <Content>
                <View style={{flex:1,borderColor:'#d6d8db',
                                borderWidth:0.5,
                                paddingVertical:responsiveHeight(4),
                                marginTop:responsiveHeight(2)}}>

                    <Text style={[Styles.fontSize_2,
                                    Styles.fontBold,
                                    {color:'black',
                                    paddingLeft:responsiveWidth(4)}]}>CARD INFORMATION</Text>

                    <View style={{flexDirection:'row',
                                justifyContent:'center'}}>
                        <View style={[{flex:.3,
                                    paddingLeft:responsiveWidth(4),
                                    justifyContent:'center'}]}>
                            <Text style={[Styles.fontSize_2]}>Full Name</Text>
                            <Text style={[Styles.fontSize_1_8]}>(On Card)</Text>
                        </View>
                     <Input  style={{flex:.7,borderBottomColor:'#d6d8db',borderBottomWidth:1}}
                            placeholder={'Your name on card'}
                     />
                     </View>
                    <View style={{flexDirection:'row',
                                justifyContent:'center'}}>
                        <View style={[{flex:.3,
                                    paddingLeft:responsiveWidth(4),
                                    justifyContent:'center'}]}>
                            <Text style={[Styles.fontSize_2]}>Credit Card</Text>
                            <Text style={[Styles.fontSize_1_8]}>Number</Text>
                        </View>
                        <Input  style={{flex:.7,borderBottomColor:'#d6d8db',borderBottomWidth:1}}
                                placeholder={'Enter your credit card number'}
                        />
                     </View>
                    <View style={{flexDirection:'row',
                                justifyContent:'center'}}>
                        <View style={[{flex:.3,
                                    paddingLeft:responsiveWidth(4),
                                    justifyContent:'center'}]}>
                            <Text style={[Styles.fontSize_2]}>CVV</Text>
                            
                        </View>
                        <Input  style={{flex:.7,borderBottomColor:'#d6d8db',borderBottomWidth:1}}
                                placeholder={'Enter your last three digits'}
                        />
                     </View>
                    <View style={{flexDirection:'row',
                                justifyContent:'center'}}>
                        <View style={[{flex:.3,
                                    paddingLeft:responsiveWidth(4),
                                    justifyContent:'center'}]}>
                            <Text style={[Styles.fontSize_2]}>Exp Date</Text>
                            
                        </View>
                        <Input  style={{flex:.7,borderBottomColor:'#d6d8db',borderBottomWidth:1}}
                            placeholder={'MM/YY'}
                        />
                     </View>
                </View>
            </Content>
            <Footer style={[Styles.alignCenter,{backgroundColor:'white',
                                   }]}>
                <TouchableOpacity style={[{paddingVertical:responsiveHeight(1.5),
                                            backgroundColor:'black',
                                            borderRadius:responsiveWidth(2),
                                            elevation:10},
                                            Styles.width_50,
                                            Styles.alignCenter]}
                                            >
                            <Text style={[{color:'white'},Styles.fontSize_2]}>SAVE</Text>
                </TouchableOpacity>
            </Footer>
        </Container>
    );
  }
}

export default AddCreditCard;
