import React, { Component } from 'react';
import { View, Text,Image } from 'react-native';
import { Container, Content } from 'native-base';
import ShoeHeader from '../../../components/ShoeHeader';
import Styles from '../../common/style'


class AddMoreSizes extends Component {

    static navigationOptions = ({
        header:null
    });

  constructor(props) {
   
    super(props);
    let img = this.props.navigation.getParam('img_src');
    this.state = {
        img_src:img
    };
  }

  render() {
    return (
      <Container>
          <ShoeHeader 
           headerTitle={'AIR JORDAN 1 RETRO OG'}
           leftIconName={'left'}
           leftIconType = {'AntDesign'}
           leftIconFn = {()=>this.props.navigation.navigate('ProductDesc')}
          />
          <Content>
              <View style={[{flex:.1},Styles.alignCenter]}>
                <Image 
                    source={this.state.img_src}
                    resizeMode={'contain'}
                    style={[Styles.width_60,
                            Styles.height_20,
                            {borderColor: '#000',
                                borderWidth:1}]}
                />
                </View>
          </Content>
      </Container>
    );
  }
}

export default AddMoreSizes;
