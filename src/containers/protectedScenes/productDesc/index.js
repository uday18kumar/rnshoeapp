import React, { Component } from 'react';
import { View, Text,Image,StyleSheet,TouchableNativeFeedback,TouchableOpacity } from 'react-native';
import { Container, Content, Icon, ListItem ,Button} from 'native-base';
import ShoeHeader from '../../../components/ShoeHeader';
import Styles from '../../common/style'
import {responsiveWidth, responsiveHeight, responsiveFontSize} from 'react-native-responsive-dimensions'
import { ScrollView } from 'react-native-gesture-handler';
import Modal from 'react-native-modalbox';
import {ShoesArray} from '../../../constants/constants'

// for image pick from file storage
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Image',
  };
  
class ProductDesc extends Component {

  static navigationOptions = ({
      header:null
  });  

  constructor(props) {
    super(props);
    let img = this.props.navigation.getParam('img_src');
    this.state = {
        img_src : img,
        photoSelected:false,
        avatarSource:null
    };
  }

  render() {
    return (
        <Container>
            <ShoeHeader 
                leftIconName = {'left'}
                leftIconType = {'AntDesign'}
                leftIconFn = {()=>this.props.navigation.navigate('Product')}
            />
            <Content>
                <View style={[{flex:.3},Styles.alignCenter]}>
                    <Image source={this.state.img_src} 
                        resizeMode={'contain'}
                        style={[Styles.width_40,Styles.height_30]}
                    />
                </View>

                <View style={[{flex:.2},Styles.alignCenter]}>
                    <Text style={[Styles.fontSize_3,
                                Styles.fontBold,
                                {color:'#000'}]}>AIR JORDAN 1 RETRO HIGH OG</Text>
                    <View style={[Styles.width_60]}>
                        <Text style={[Styles.fontSize_2,{textAlign:'center'}]}>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        </Text>
                        <View style={[{flexDirection:'row'},Styles.height_5,
                                        Styles.alignCenter,Styles.justifyCenter]}>
                            <Text style={[Styles.fontSize_2,
                                            Styles.fontBold,
                                            Styles.textUnderline,
                                            {color:'#000',
                                            textAlign:'center',
                                            }]}>READ MORE</Text>
                            <Icon name={'share-alt'} type={'FontAwesome'}  
                                style={[Styles.fontSize_2,
                                        {textAlign:'center',
                                        paddingLeft:responsiveWidth(2)}]}
                            />
                        </View>
                    </View>
                
                </View>
                <View style={{flex:.5}}>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>SKU</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>ABCD-EDFGH-23</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>NickName</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>'Grim Rapers'</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>Main Color</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>Black</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>Designer</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>Abcd</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>Brand</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>Adidas</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>Category</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>LifeStyle</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={[{flex:.5,color:'#000'},Styles.fontSize_2]}>Pair</Text>
                        <Text style={[{flex:.5,textAlign:'right'},Styles.fontSize_2]}>1</Text>
                    </ListItem>
                </View>
                <View style={Styles.paddingTop_2}>
                    <Text style={[Styles.fontBold,Styles.fontSize_3,
                            {color:'#000',
                            paddingLeft:responsiveWidth(3)}]}>
                    RELATED PRODUCTS</Text>
                </View>
                <ScrollView horizontal={true} style={[Styles.height_18,{borderColor:'grey',borderWidth:0.25}]}>
                        {this.renderShoes()}
                </ScrollView>


                <View style={[{
                                flexDirection:'row',
                                justifyContent:'space-evenly',
                                paddingBottom: responsiveHeight(2),
                                borderTopWidth: 0.5,
                                borderTopColor: 'grey',    
                                marginTop: responsiveHeight(3)    
                                }
                                ]}>
                        <TouchableNativeFeedback onPress={()=>this.refs.ownShoeModal.open()}>
                        <View style={styles.buttonStyle}>
                            <Icon name={'shoe-formal'} type={'MaterialCommunityIcons'} 
                                  style={{color:'#fff',fontSize:responsiveFontSize(3)}}
                            />
                            <Text style={styles.buttonTextStyle}>OWN</Text>
                        </View>
                        </TouchableNativeFeedback>
                        
                        <TouchableNativeFeedback>
                        <View style={styles.buttonStyle}>
                        <Icon name={'shoe-formal'} type={'MaterialCommunityIcons'} 
                                style={{color:'#fff',fontSize:responsiveFontSize(3)}}
                            />
                            <Text style={styles.buttonTextStyle}>TRADE</Text>
                        </View>
                        </TouchableNativeFeedback>
                        
                        <TouchableNativeFeedback>
                        <View style={styles.buttonStyle}>
                            <Icon name={'shoe-formal'} type={'MaterialCommunityIcons'} 
                                style={{color:'#fff',fontSize:responsiveFontSize(3)}}
                            />
                            <Text style={styles.buttonTextStyle}>WANT</Text>
                        </View>
                        </TouchableNativeFeedback>

                </View>

            </Content>
            <Modal
                    style={[styles.modal]}
                    ref={"ownShoeModal"}
                    swipeToClose={false}
                    onClosed={()=>this.modalClose()}
                    position={'center'}
                    backdropPressToClose={false}
                    >
                        <View style={[{flex:.1},
                                    Styles.alignCenter,
                                    
                                    {flexDirection:'row',
                                        borderBottomColor: 'grey',
                                        borderBottomWidth:1}]}
                                >
                            <Text style={[{flex:1,textAlign:'center',color:'#000'},Styles.fontBold,
                                            Styles.fontSize_2_5]}>SNEAKERS ADDED</Text>
                            <TouchableNativeFeedback onPress={()=>this.modalClose()}>
                                <Icon name={'close'} type={'MaterialIcons'}
                                    style={[
                                        Styles.fontSize_3]} />
                            </TouchableNativeFeedback>
                        </View>
                        <View style={{flex:0.1}} />
                        <View style={[{flex:.2},
                                        Styles.alignCenter,
                                        Styles.justifyCenter]}>
                                <Image source={this.state.img_src} 
                                    resizeMode={'contain'}
                                    style={[Styles.height_20,Styles.width_30]}
                                />
                        </View>
                 
                        <View style={[{flex:.3,borderBottomColor:'grey',
                                        borderBottomWidth:0.5},
                                    Styles.alignCenter,
                                    Styles.justifyCenter]}>
                            <Text style={[Styles.fontSize_2,
                                        Styles.fontBold,
                                        {color:'#000'}]}>AIR JORDAN 1 RETRO HIGH OG</Text>
                            <Text style={[Styles.fontSize_2,
                                        Styles.fontBold,
                                        ]}>SKU - AOHLBC</Text>
                            <Text style={[Styles.fontSize_2,
                                        Styles.fontBold,
                                        ]}>SIZE - 7</Text>
                        </View>

                        <View style={[{flex:.1,flexWrap:'wrap',alignSelf: 'center',},
                                        Styles.width_60,
                                        Styles.alignCenter]}>
                            <Text style={[{textAlign:'center'},Styles.fontSize_1_5]}>
                                Add the sizes and photos you own to add
                                the sneakers to your proifle for easy sellings 
                            </Text>
                        </View>

                        <View style={[{flex:.3},Styles.alignCenter]}>
                                <TouchableOpacity
                                    onPress={()=>this.addImage()}
                                   style={[Styles.width_50,
                                            Styles.alignCenter,
                                            Styles.justifyCenter,
                                            {backgroundColor:'#000',
                                            borderRadius:responsiveWidth(1),
                                                flex:.3,
                                                }]} >
                                    <View style={{flexDirection:'row'}}>
                                        <Icon name={'camera'}
                                            style={[styles.modalBtnTextStyle,
                                                    {paddingRight: responsiveWidth(2)}]} />
                                        <Text style={styles.modalBtnTextStyle}>
                                                    ONFEET - ADD PHOTOS</Text>
                                    </View>
                                    <Text style={[Styles.fontSize_1,{color:'#fff'}]}>(Show off how you wear yours)</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={()=>this.props.navigation.navigate('AddMoreSizes',{'img_src':this.state.img_src})}
                                   style={[Styles.width_50,
                                            Styles.alignCenter,
                                            Styles.justifyCenter,
                                            {backgroundColor:'#000',
                                            borderRadius:responsiveWidth(1),
                                                flex:.3,
                                                marginTop:responsiveHeight(1.5),
                                                flexDirection:'row'}]} >
                                    <Icon name={'add'}
                                         style={[styles.modalBtnTextStyle,
                                                {paddingRight: responsiveWidth(2)}]} />
                                    <Text style={styles.modalBtnTextStyle}>
                                                 ADD MORE SIZES</Text>
                                </TouchableOpacity>
                        </View>
                    </Modal>
        </Container>
    );
  }

  modalClose(){
    this.refs.ownShoeModal.close();
  }

  renderShoes(){
        return ShoesArray.map((item)=>{
            return(
                <View style={[Styles.alignCenter,
                            {flexWrap:'wrap',
                            borderColor: 'grey',
                            borderWidth: 0.25},
                            Styles.width_30]}>
                    <Image 
                        source={item.img}
                        resizeMode={'contain'}
                        style={[Styles.width_20,Styles.height_10]}
                    />
                    <Text style={[Styles.fontSize_1_5,
                                {textAlign:'center'}]}>
                                    {item.text}</Text>
                </View>
            );
        })
  }

  addImage(){
    ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
    
        if (response.didCancel) {
        alert('User cancelled image picker');
        } else if (response.error) {
        alert('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
        alert('User tapped custom button: ', response.customButton);
        } else {
       
        const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
            avatarSource: source,
            photoSelected:true
        });
        }
    });
    }

}

const styles = StyleSheet.create({
    buttonStyle:{
            backgroundColor:'#000',
            height:responsiveHeight(8.5),
            width:responsiveWidth(14),
            borderRadius: responsiveHeight(8),
            alignItems: 'center',
            justifyContent:'center',
            elevation:7,
            
            marginTop:responsiveHeight(-2)
    },
    buttonTextStyle:{
        color:'#fff',
        fontSize:responsiveFontSize(1.5)
    },
    modalBtnTextStyle:{
        color:'#fff',
        fontSize:responsiveFontSize(2)
    },
    modal:{
        height:responsiveHeight(65),
        width:responsiveWidth(90)
    }
})

export default ProductDesc;
