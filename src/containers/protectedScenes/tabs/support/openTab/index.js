import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container, Content } from 'native-base';
import Styles from '../../../../common/style'


class OpenTab extends Component {

  static navigationOptions = ({
      header:null
  });  

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Container>
          <Content contentContainerStyle={[{flex:1},
                                        Styles.alignCenter,
                                        Styles.justifyCenter]}>
            <Text>
                Open Tab
            </Text>
          </Content>
        </Container>
    );
  }
}

export default OpenTab;
