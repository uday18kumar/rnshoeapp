import React, { Component } from 'react';
import { View, Text ,TouchableOpacity} from 'react-native';
import {Icon} from 'native-base';
import {createStackNavigator} from 'react-navigation'
import Styles from '../../../common/style'
import { responsiveWidth } from 'react-native-responsive-dimensions';
import ShoeHeader from '../../../../components/ShoeHeader'
import {Container,Content} from 'native-base';
import SupportTabNavigator from '../../../../routers/SupportTabNavigator';



class Support extends Component {

  static navigationOptions = ({
    tabBarVisible:false
  });

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <ShoeHeader 
          headerTitle={'YOUR TICKETS'}
          leftIconName={'left'}
          leftIconType = {'AntDesign'}
          leftIconFn = {()=>this.props.navigation.navigate('Product')}
          rightText = {'CREATE YOUR TICKET'}
          rightTextFn = {()=>{alert('Hello')}}
        />
        <SupportTabNavigator />
      </Container>
    );
  }
}

export default Support;
