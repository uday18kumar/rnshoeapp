import React, { Component } from 'react';
import { View, Text ,TouchableOpacity,StyleSheet,ScrollView} from 'react-native';
import {Icon, Input, ListItem} from 'native-base';
import {createStackNavigator} from 'react-navigation'
import Styles from '../../../common/style'
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import ShoeHeader from '../../../../components/ShoeHeader'
import {Container,Content} from 'native-base';
import SearchInput, { createFilter } from 'react-native-search-filter';
import {BrandsArray} from '../../../../constants/constants'

const KEYS_TO_FILTERS = ['name'];
class Search extends Component {

  static navigationOptions = ({
    tabBarVisible:false
  });
  constructor(props) {
    super(props);
    this.state = {
        searchTerm:''
    };
  }

  render() {
    const filteredBrands = BrandsArray.filter(createFilter(this.state.searchTerm,KEYS_TO_FILTERS))
    return (
      
      <Container>
        <ShoeHeader 
          headerTitle={'SEARCH'}
          leftIconName={'left'}
          leftIconType = {'AntDesign'}
          leftIconFn = {()=>this.props.navigation.navigate('Product')}
        />
         <Content contentContainerStyle={[{flex:1}]} >
            <View>
              <SearchInput 
                onChangeText={(term) => { this.searchUpdated(term) }} 
                style={styles.searchInput}
                placeholder="Search by name & SKU"
            />
            </View>
            <View style={[{flex:.1},Styles.justifyCenter]}>
            <Text style={[Styles.fontSize_2,
                          Styles.fontBold,
                          {color:'#000',
                          paddingLeft:responsiveWidth(3)}]}>
                POPULAR BRANDS</Text>
            </View>


          <ScrollView>
          {filteredBrands.map((brand) => {
            return (
              <TouchableOpacity>
                <ListItem>
                  <Icon name={'shoe-formal'} 
                        type={'MaterialCommunityIcons'} 
                        style={{paddingRight: responsiveWidth(4)}}
                        />
                  <Text style={[Styles.fontSize_2]}>
                        {brand.name}
                    </Text>
                </ListItem>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
        </Content>
      </Container>
    );
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }
}

const styles = StyleSheet.create({
  searchInput:{
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
    textAlign:'center',
    alignSelf: 'center',
    width:responsiveWidth(90),
    borderRadius: responsiveWidth(2),
    marginVertical: responsiveHeight(2)
  }
})

export default Search;