import React, { Component } from 'react';
import { View, Text ,TouchableOpacity} from 'react-native';
import {Icon} from 'native-base';
import {createStackNavigator} from 'react-navigation'
import Styles from '../../../common/style'
import { responsiveWidth } from 'react-native-responsive-dimensions';
import ShoeHeader from '../../../../components/ShoeHeader'
import {Container,Content} from 'native-base';
import StarRatingView from '../../../../components/starRating';


class Profile extends Component {

  static navigationOptions = ({
    tabBarVisible:false
  });
  
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <ShoeHeader 
          headerTitle={'MY PROFILE'}
          leftIconName={'left'}
          leftIconType = {'AntDesign'}
          leftIconFn = {()=>this.props.navigation.navigate('Product')}
          rightIconName1={'bell'}
          rightIconName2={'dots-three-horizontal'}
          rightIconType1 = {'MaterialCommunityIcons'}
          rightIconType2 = {'Entypo'}
          rightIconFn2 = {()=>this.props.navigation.navigate('Settings')}
        />
        <Content contentContainerStyle={[{flex:1},
                                        Styles.alignCenter,
                                        Styles.justifyCenter]}>
          <Text>Profile</Text>

            <StarRatingView 
                full_StarColor={'#A57BB6'}
                empty_StarColor={'#000000'}
                fnDisabled={true}
                rating={3}
            />
        </Content>
      </Container>
    );
  }
}

export default Profile;
