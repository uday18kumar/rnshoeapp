import React, { Component } from 'react';
import { View, Text,TouchableOpacity,Image } from 'react-native';
import {Icon,Container,Content} from 'native-base';
import { createAppContainer,createStackNavigator } from 'react-navigation';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../../common/style'

// shoe header
import ShoeHeader from '../../../../components/ShoeHeader'

import {ShoesArray} from '../../../../constants/constants'
class ProductDashboard extends Component {


  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Container>
            <ShoeHeader 
                headerTitle = {'PRODUCTS'}
                rightIconName1 = {'filter'}
                rightIconType1 = {'FontAwesome'}  
                rightIconFn1 = {()=>this.props.navigation.navigate('Filter')}
            />
             <Content>
                <View style={{flexWrap:'wrap',flexDirection:'row'}}>
                    {this.renderShoes()}
                    </View>
                
            </Content>
        </Container>
    );
  }

  renderShoes(){
    return ShoesArray.map((item,index)=>{
      return (
        <TouchableOpacity onPress={()=>this.props.navigation.navigate('ProductDesc',{'img_src':item.img})}>
        <View style={[{
          width:responsiveWidth(50),
          paddingTop:responsiveHeight(3),
          borderColor:'#B7C2CA',
          borderWidth:.2},
          Styles.alignCenter]}>

          <View style={{width:responsiveWidth(30)}}>
            <Text style={[{textAlign:'center',
                        color:'#000'},
                        Styles.fontBold,
                        Styles.fontSize_1_8]}>
                  {item.text}{index}</Text>
          </View>
          <View style={{flexDirection:'row',paddingTop:responsiveHeight(2)}}>
          <Icon name={'heart-circle'} type={'MaterialCommunityIcons'} 
            style={[Styles.fontSize_2,{marginHorizontal:responsiveWidth(2)}]}
          />
          <Icon name={'thumb-up'} type={'MaterialCommunityIcons'} 
            style={[Styles.fontSize_2]}
          />
          </View>
          <Image source={item.img}
            resizeMode={'contain'}
            style={[Styles.width_30,
                Styles.height_20]} />
          </View>
          </TouchableOpacity>
      );
    })
  }
}


export default ProductDashboard;