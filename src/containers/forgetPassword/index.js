import React, { Component } from 'react';
import { View, Text,TouchableOpacity,Button } from 'react-native';
import Styles from '../common/style'
import {Input,Container,Content} from 'native-base'
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';

class ForgetPassword extends Component { 

    static navigationOptions = ({
        headerTransparent:true
    });

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Container >
            <Content contentContainerStyle={[{flex: 1,
                                marginTop:responsiveHeight(20)},
                                Styles.alignCenter,
                                ]}>
            <Text style={[Styles.fontSize_1_5]}>
                We just need your registered email address
            </Text>
            <Text style={[Styles.fontSize_1_5]}>
                to send you password reset
            </Text>

            <View style={[Styles.width_90,Styles.height_13,
                {backgroundColor:'#302E2E',borderRadius:responsiveWidth(2),
                marginTop:responsiveHeight(7)}]}>
                <Input 
                    placeholder={'Email address'}
                    placeholderTextColor={'white'}
                    style={[{color:'white',paddingLeft:responsiveWidth(4)},Styles.fontSize_1_5]}
                />
            </View>

            <TouchableOpacity style={[Styles.width_90,
                                    Styles.alignCenter,
                                {backgroundColor:'grey',
                                    borderRadius:responsiveWidth(2),
                                    marginTop:responsiveHeight(3)}]}>
                <Text style={[Styles.fontSize_1_5,
                            {padding:responsiveWidth(3),color:'white'}]}>
                    RESET PASSWORD
                </Text>
            </TouchableOpacity>
            </Content>
        </Container>
    );
  }
}

export default ForgetPassword;
