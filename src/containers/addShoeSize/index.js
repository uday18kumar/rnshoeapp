import React, { Component } from 'react';
import { View, Text,ScrollView,TouchableOpacity } from 'react-native';
import {Container,Content,Footer} from 'native-base'
import Styles from '../common/style';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

const usSizes = ['6','6.5','7','7.5','8','8.5','9','9.5','10','10.5','11','11.5','12','13','14'];
const ukSizes = ['5.5','6','6.5','7','7.5','8','8.5','9','9.5','10','10.5','11','11.5','12.5','13.5','14.5'];
const euSizes = ['39','39','40','40-41','41','41-42','42','42-43','43-44','43-44.5','44-45','45-46','46-47','47-48','48-49'];


class AddShoeSize extends Component {
    static navigationOptions = {
        headerTransparent:true
    };
      
      
  constructor(props) {
    super(props);
    this.state = {
        shoeSizeCode:'US',
        shoeSizes:[]
    };
  }

  render() {
    return (
        <Container>
            <Content contentContainerStyle={[Styles.alignCenter]}>
                <Text style={[Styles.fontSize_2,Styles.fontBold,
                                {paddingTop:responsiveHeight(8),
                                color:'black',
                                elevation:3}]}>
                    ADD SIZES
                </Text>
                <Text style={[Styles.fontSize_1_8,
                                {paddingTop:responsiveHeight(3),
                                color:'black'}]}>
                    Tell us your ideal sneaker sizes
                </Text>
                <Text style={[Styles.fontSize_2,{paddingTop:responsiveHeight(5)}]}>
                    SNEAKERS
                </Text>

                <View style={[{justifyContent:'space-between',
                                flexDirection:'row',
                                paddingTop:responsiveHeight(3)},
                                Styles.width_60]}>
                    <TouchableOpacity onPress={()=>this.setState({shoeSizeCode:'UK'})}>
                        <View style={{backgroundColor:(this.state.shoeSizeCode == 'UK') ? 'black':'grey',
                                        width:responsiveWidth(15),
                                        paddingVertical:responsiveHeight(1),
                                        alignItems:'center',
                                        borderRadius: responsiveWidth(1),
                                        }}>
                            <Text style={[Styles.fontSize_1_8,
                                            Styles.fontBold,
                                            {color:'white'}]}>UK</Text>           
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={()=>this.setState({shoeSizeCode:'US'})}>
                        <View style={{backgroundColor:(this.state.shoeSizeCode == 'US') ? 'black':'grey',
                                        width:responsiveWidth(15),
                                        paddingVertical:responsiveHeight(1),
                                        alignItems:'center',
                                        borderRadius: responsiveWidth(1),
                                        }}>
                            <Text style={[Styles.fontSize_1_8,
                                            Styles.fontBold,
                                            {color:'white'}]}>US</Text>           
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={()=>this.setState({shoeSizeCode:'EU'})}>
                        <View style={{backgroundColor:(this.state.shoeSizeCode == 'EU') ? 'black':'grey',
                                        width:responsiveWidth(15),
                                        paddingVertical:responsiveHeight(1),
                                        alignItems:'center',
                                        borderRadius: responsiveWidth(1),
                                        }}>
                            <Text style={[Styles.fontSize_1_8,
                                            Styles.fontBold,
                                        {color:'white'}]}>EU</Text>           
                        </View>
                    </TouchableOpacity>
                    
                </View>

                
                <ScrollView horizontal={true}
                            contentContainerStyle={[
                                            Styles.justifyCenter,
                                            Styles.alignCenter,
                                            {height:responsiveHeight(15),
                                            marginTop:responsiveHeight(4)},
                                            ]}  >
                            {(this.state.shoeSizeCode == 'UK') && 
                                 this.renderSizes()
                            }
                            {(this.state.shoeSizeCode == 'US') && 
                                 this.renderSizes()
                            }
                            {(this.state.shoeSizeCode == 'EU') && 
                                 this.renderSizes()
                            }
                </ScrollView>  
                      
            </Content>
            <Footer style={[Styles.alignCenter,{backgroundColor:'white',
                                   }]}>
                <TouchableOpacity style={[{paddingVertical:responsiveHeight(1.5),
                                            backgroundColor:'black',
                                            borderRadius:responsiveWidth(2),
                                            elevation:10},
                                            Styles.width_50,
                                            Styles.alignCenter]}
                                            >
                            <Text style={[{color:'white'},Styles.fontSize_2]}>DONE</Text>
                </TouchableOpacity>
            </Footer>
        </Container>
    );
  }


  renderSizes(){
      if(this.state.shoeSizeCode == 'UK')
      {
        return ukSizes.map((item)=>{
            return(
                  <TouchableOpacity style={{height:responsiveHeight(10)}}
                    onPress={()=>this.addShoeSizetoArray(item)}
                  >
                      <View style={{backgroundColor:(this.state.shoeSizes.includes(item))?'black':'white',
                          width:responsiveWidth(14),
                          height:responsiveHeight(8),
                          marginHorizontal:responsiveWidth(2),
                          alignItems:'center',
                          justifyContent:'center',
                          elevation:10,
                          borderRadius: responsiveWidth(1),
                          }} > 
                          <Text style={[Styles.fontSize_2,
                                          Styles.fontBold,
                                          {color:(this.state.shoeSizes.includes(item))?'white':'black'}]}>
                              {item}
                          </Text>           
                      </View>
                  </TouchableOpacity>
            );
        });
      }
      else if(this.state.shoeSizeCode == 'US'){
        return usSizes.map((item)=>{
            return(

              <TouchableOpacity style={{height:responsiveHeight(10)}}
                        onPress={()=>this.addShoeSizetoArray(item)}
              >
                  <View style={{backgroundColor:(this.state.shoeSizes.includes(item))?'black':'white',
                      width:responsiveWidth(14),
                      height:responsiveHeight(8),
                      marginHorizontal:responsiveWidth(2),
                      alignItems:'center',
                      justifyContent:'center',
                      elevation:10,
                      borderRadius: responsiveWidth(1),
                      }} > 
                      <Text style={[Styles.fontSize_2,
                                      Styles.fontBold,
                                      {color:(this.state.shoeSizes.includes(item))?'white':'black'}]}>
                          {item}
                      </Text>           
                  </View>
                  </TouchableOpacity>
            );
        });
      }
      else if(this.state.shoeSizeCode == 'EU'){
        return euSizes.map((item)=>{
            return(
              <TouchableOpacity style={{height:responsiveHeight(10)}}
              onPress={()=>this.addShoeSizetoArray(item)}
              >
                  <View style={{backgroundColor:(this.state.shoeSizes.includes(item))?'black':'white',
                      width:responsiveWidth(14),
                      height:responsiveHeight(8),
                      marginHorizontal:responsiveWidth(2),
                      alignItems:'center',
                      justifyContent:'center',
                      elevation:10,
                      borderRadius: responsiveWidth(1),
                      }} > 
                      <Text style={[Styles.fontSize_1_8,
                                      Styles.fontBold,
                                      {color:(this.state.shoeSizes.includes(item))?'white':'black'}]}>
                          {item}
                      </Text>           
                  </View>
                  </TouchableOpacity>
            );
        });
      }  
  }

  addShoeSizetoArray(value)
  {
      let shoeArray = this.state.shoeSizes;
      if(!shoeArray.includes(value))
      {
            shoeArray.push(value);
      }
      else
      {
          shoeArray.splice(shoeArray.indexOf(value),1);
      }
      this.setState({shoeSizes:shoeArray});
  }

}

export default AddShoeSize;
