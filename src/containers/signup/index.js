import React, { Component } from 'react';
import { View, Text,TouchableOpacity,TouchableHighlight,Image,ToastAndroid } from 'react-native';
import {Container,Content,Icon,Item,Input} from 'native-base';
import Styles from '../common/style'
import {responsiveFontSize,responsiveHeight,responsiveWidth} from 'react-native-responsive-dimensions';

// for instagram login
import InstagramLogin from 'react-native-instagram-login'

// for facebook login
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase'

// for image pick from file storage
import ImagePicker from 'react-native-image-picker';


//for validation
import {validateEmail, validatePassword} from '../../util/validation'
const options = {
    title: 'Select Image',
  };

class SignUp extends Component {
    static navigationOptions = {
        header:null
      };
      
  constructor(props) {
    super(props);
    this.state = {
        avatarSource: null,
        photoSelected:false,
        email:'',
        password:'',
        username:'',
        fullname:'',
        size:null
    };
  }

  render() {
    return (
        <Container>
            <Content contentContainerStyle={[Styles.width_100,
                                            Styles.alignCenter]}>
                <View style={[Styles.width_90,
                                Styles.alignCenter,
                                Styles.paddingTop_10]}>
                    <Text style={[Styles.fontSize_3,
                                Styles.fontBold]}>
                                DEFINED BY SOLE
                    </Text>
                </View>


                {/* sign in methods */}
                <View style={[Styles.width_90,
                                Styles.alignCenter,
                                {flexDirection:'row',
                                borderColor: 'black',
                                borderWidth: 1,
                                borderRadius: responsiveHeight(2),
                                marginTop:responsiveHeight(5),
                                paddingVertical: responsiveHeight(1)}]}>

                        <View style={{width:responsiveWidth(60),paddingLeft:responsiveWidth(5)}}>
                                <Text style={[Styles.fontSize_2]}>SIGN IN</Text>
                        </View>
                        <View style={{width:responsiveWidth(15)}}>
                            <TouchableOpacity onPress={()=>this.facebookLogin()}>
                                <Icon name={'facebook'} type={'FontAwesome'}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:responsiveWidth(15)}}>
                            <TouchableOpacity onPress={() => this.refs.instagramLogin.show()}>
                                <Icon name={'instagram'} type={'FontAwesome'}/>
                            </TouchableOpacity> 
                        </View>
                </View>   

                 <View style={[Styles.alignCenter,
                    {paddingVertical:responsiveHeight(2)}]}>
                    <Text style={[Styles.fontSize_1_5]}>OR</Text>
                </View>


                    {/* sign up fields */}
                <View style={[
                            Styles.justifyCenter,
                            {backgroundColor:'#232124',
                            borderRadius:responsiveWidth(2),
                            flex:.7}]}>
                <View style={[Styles.width_90,
                            Styles.alignCenter,
                            Styles.justifyCenter,  
                            {
                                paddingTop:responsiveHeight(2)
                            } 
                            ]}>
                    {(!this.state.photoSelected) ? 
                    (
                    <TouchableOpacity onPress={()=>this.addImage()} 
                        style={Styles.alignCenter}>
                        <Icon name={'ios-add-circle-outline'} type={'Ionicons'} 
                                style={{color:'white',
                                fontSize:responsiveFontSize(12)
                                }}
                        />
                        <Text style={[{color:'white'},Styles.fontSize_1_5]}>Add Photo</Text>
                    </TouchableOpacity>
                    ):(
                        <View >
                            <Image 
                                source={this.state.avatarSource}
                                style={{width:responsiveWidth(25),
                                        height:responsiveHeight(15),
                                        borderRadius:responsiveWidth(15)
                                        }}
                            />
                        </View>
                    )}
                    </View>
                    <Item>
                        <Input 
                            placeholder={'EMAIL'}
                            placeholderTextColor={'white'}
                            style={{color:'white',
                                    height:responsiveHeight(10),
                                    fontSize:responsiveFontSize(1.5),
                                    paddingLeft:responsiveWidth(3)
                                }}
                            value={this.state.email}
                            onChangeText={(text)=>this.setState({email:text})}
                        />
                    </Item>
                    <Item >
                        
                        <Input 
                            placeholder={'PASSWORD'}
                            placeholderTextColor={'white'}
                            secureTextEntry
                            style={{color:'white',
                            height:responsiveHeight(10),
                            fontSize:responsiveFontSize(1.5),
                            paddingLeft:responsiveWidth(3)
                        }}
                        value={this.state.password}
                        onChangeText={(text)=>this.setState({password:text})}
                        />
                    </Item>
                    <Item >
                        <Input 
                            placeholder={'USERNAME'}
                            placeholderTextColor={'white'}
                            style={{color:'white',
                                    height:responsiveHeight(10),
                                    fontSize:responsiveFontSize(1.5),
                                    paddingLeft:responsiveWidth(3)
                                }}
                        />
                    </Item>
                    <Item >
                        <Input 
                            placeholder={'FULLNAME'}
                            placeholderTextColor={'white'}
                            style={{color:'white',
                                    height:responsiveHeight(10),
                                    fontSize:responsiveFontSize(1.5),
                                    paddingLeft:responsiveWidth(3)
                                }}
                        />
                    </Item>

                    <TouchableOpacity
                     onPress={()=>this.props.navigation.navigate('AddShoeSize')} >
                    <View style={{
                                width:responsiveWidth(90),
                                height:responsiveHeight(10),
                                alignItems:'center',
                                flexDirection:'row',
                                  }}>
                        <Text
                            style={{color:'white',
                                    flex:.95,
                                    fontSize:responsiveFontSize(1.5),
                                    paddingLeft:responsiveWidth(3),

                                }}
                        >SIZE</Text>
                        <Icon 
                            name={'chevron-down'}
                            type={'Entypo'}
                            style={[Styles.fontSize_1_8,
                                    {
                                        color:'white',
                                        justifyContent: 'flex-end',
                                    }]}
                        />
                  
                    </View>
                    </TouchableOpacity>
      
            </View>

            {/* sign up button    */}
            
            <TouchableOpacity 
                onPress={()=>this.onSignUp()}
               style={[Styles.width_90,
                        Styles.alignCenter,
                        {backgroundColor:'black',
                        borderRadius:responsiveWidth(2),
                        paddingVertical:responsiveHeight(2),
                        marginTop:responsiveHeight(2)}]} >
                    <Text style={[Styles.fontSize_2,
                                {color:'white'}]}>
                        SIGN UP
                    </Text>
            </TouchableOpacity>

            {/* already have an account */}
              
            <View style={[Styles.width_90,
                        {
                            alignItems: 'center',
                            paddingTop:responsiveHeight(1),
                            paddingBottom:responsiveHeight(2)
                        }
                        ]}>
                             <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}> 
                    <Text style={[Styles.fontSize_1_5]}>
                        Already have an account?
                    <Text style={[Styles.fontSize_1_8,
                                    Styles.fontBold,
                                     ]}>
                        SIGN IN
                    </Text>
                </Text>
                </TouchableOpacity>
            </View>


            <InstagramLogin
                        ref= {'instagramLogin'}
                        clientId='ee73264fc1254622a2d396c50ded4744' //pass the client id which is generated from instagram developer account
                        redirectUrl='http://sachtechsolution.com/' // pass the redirect url we put it in the instagram valid redirect url when generating client id
                        scopes={['public_content', 'follower_list']}
                        onLoginSuccess={(code) => {alert("Success Token :- " + code)}}
                        onLoginFailure={(data) => {alert("Failure :- "+data)}}
            />
            </Content>
        </Container>
    );
  }


//   facebook login function
  async facebookLogin() {
    try {
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
  
      if (result.isCancelled) {
        // handle this however suites the flow of your app
        throw new Error('User cancelled request'); 
      }
  
      console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
  
      // get the access token
      const data = await AccessToken.getCurrentAccessToken();
  
      if (!data) {
        // handle this however suites the flow of your app
        throw new Error('Something went wrong obtaining the users access token');
      }
  
      // create a new firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
  
      // login with credential
      const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
      
      alert(JSON.stringify(firebaseUserCredential.user.toJSON()))
    } catch (e) {
      alert(e);
    }
  }


//   image function
    addImage(){
    ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
    
        if (response.didCancel) {
        alert('User cancelled image picker');
        } else if (response.error) {
        alert('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
        alert('User tapped custom button: ', response.customButton);
        } else {
       
        const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
            avatarSource: source,
            photoSelected:true
        });
        }
    });
    }


    // sign up button
    onSignUp()
    {
        if(!validateEmail(this.state.email)){
            ToastAndroid.show('Email is not valid',ToastAndroid.LONG);
        }
        if(!validatePassword(this.state.password)){
            ToastAndroid.show('Password must be greater than 6 characters',ToastAndroid.LONG);
        }
        else{
            alert(
                this.state.email + ':' + this.state.password
            )
        }
    }
}

export default SignUp;
