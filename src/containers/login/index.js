import React, { Component } from 'react';
import { View, Text,TouchableOpacity,ToastAndroid } from 'react-native';
import Styles from '../common/style'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import {Icon,Input, Item, Label} from 'native-base'

// for instagram login
import InstagramLogin from 'react-native-instagram-login'

// for facebook login
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase'

// validation class
import {validatePassword} from '../../util/validation'

// loader
import {Bars} from 'react-native-loader';

// Star Rating
import StarRatingView from '../../components/starRating';

class Login extends Component {
    static navigationOptions = {
          header:null
      };
      
  constructor(props) {
    super(props);
    this.state = {
        user_name:'',
        user_password:'',
        loader:false,
    };
  }



  render() 
  {
    return (
      <View style={[Styles.alignCenter]}>
          
          <View style={[Styles.alignCenter,{paddingTop:responsiveHeight(15)}]}>
              <Text style={[Styles.fontSize_3,Styles.fontBold]}>DEFINED BY SOLE</Text>
          </View>

            {/* <StarRatingView 
                full_StarColor={'#A57BB6'}
                empty_StarColor={'#000000'}
                fnDisabled={true}
                rating={3}
            /> */}

          <View style={[Styles.width_90,
                        Styles.alignCenter,
                        {flexDirection:'row',
                        borderColor: 'black',
                        borderWidth: 1,
                        borderRadius: responsiveHeight(2),
                        marginTop:responsiveHeight(5),
                        paddingVertical: responsiveHeight(1)}]}>

                <View style={{width:responsiveWidth(60),paddingLeft:responsiveWidth(5)}}>
                        <Text style={[Styles.fontSize_2]}>SIGN IN</Text>
                </View>
                <View style={{width:responsiveWidth(15)}}>
                    <TouchableOpacity onPress={()=>this.facebookLogin()}>
                        <Icon name={'facebook'} type={'FontAwesome'}/>
                    </TouchableOpacity>
                </View>
                <View style={{width:responsiveWidth(15)}}>
                <TouchableOpacity onPress={() => this.refs.instagramLogin.show()}>
                    <Icon name={'instagram'} type={'FontAwesome'}/>
                </TouchableOpacity>
                
                </View>
          </View>

            <View style={[Styles.alignCenter,{paddingVertical:responsiveHeight(2)}]}>
                <Text style={[Styles.fontSize_1_5]}>OR</Text>
            </View>
            
            <View style={[Styles.width_90,Styles.justifyCenter,{backgroundColor:'#232124',
                            height:responsiveHeight(20),
                            borderRadius:responsiveWidth(1)}]}>
                <Item >
                        <Input 
                            placeholder={'USERNAME'}
                            placeholderTextColor={'white'}
                            style={{color:'white',
                                    height:responsiveHeight(10),
                                    fontSize:responsiveFontSize(1.5),
                                    paddingLeft:responsiveWidth(3)
                                }}
                            value={this.state.user_name}
                            onChangeText={(text)=>this.setState({user_name:text})}
                        />
                    </Item>
                    <Item 
                    style={{borderBottomWidth:0}}
                   >
                        
                        <Input 
                            placeholder={'PASSWORD'}
                            placeholderTextColor={'white'}
                            secureTextEntry
                            style={{color:'white',
                            height:responsiveHeight(10),
                            fontSize:responsiveFontSize(1.5),
                            paddingLeft:responsiveWidth(3)
                        }}
                        value={this.state.user_password}
                        onChangeText={(text)=>this.setState({user_password:text})}
                        />
                    </Item>
            </View>

            <View style={[Styles.width_90,{alignItems:'flex-end',paddingBottom:responsiveHeight(2)}]}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('ForgetPassword')}>
                    <Text style={[Styles.fontSize_1_5]}>Forgot Password?</Text>
                </TouchableOpacity>
            </View>

            <TouchableOpacity 
                onPress={()=>this.onSignIn()}
               style={[Styles.width_90,
                        Styles.alignCenter,
                        {backgroundColor:'black',
                        borderRadius:responsiveWidth(2),
                        paddingVertical:responsiveHeight(2),
                        marginTop:responsiveHeight(2)}]} >
                    <Text style={[Styles.fontSize_2,
                                {color:'white'}]}>
                        SIGN IN
                    </Text>
            </TouchableOpacity>
               
            <View style={[Styles.width_90,
                        {
                            alignItems: 'center',
                            paddingTop:responsiveHeight(4)
                        }
                        ]}>
                             <TouchableOpacity onPress={()=>this.props.navigation.navigate('SignUp')}> 
                    <Text style={[Styles.fontSize_1_5]}>
                        Don't have an account?
                  
                    <Text style={[Styles.fontSize_1_8,
                                    Styles.fontBold,
                                     ]}>
                        SIGN UP
                    </Text>
          
                </Text>
                </TouchableOpacity>
            </View>
                    
            <View style={[Styles.alignCenter,{paddingVertical:responsiveHeight(5)}]}>
                
                <Text style={[Styles.fontSize_1_8,Styles.textUnderline]}>
                    CONTINUE AS GUEST
                </Text>
            </View>
            

            {(this.state.loader) && 
                <View style={[{
                                flex: 1,
                                position: 'absolute',
                                left: 0,
                                top: 0,
                                opacity: 0.8,
                                backgroundColor: 'black',
                                width: responsiveWidth(100),
                                height:responsiveHeight(100) 
                        },
                            Styles.alignCenter,
                            Styles.justifyCenter]}>

                    <Bars size={10} color="white" />
                </View>
            }

            <InstagramLogin
                        ref= {'instagramLogin'}
                        clientId='ee73264fc1254622a2d396c50ded4744' //pass the client id which is generated from instagram developer account
                        redirectUrl='http://sachtechsolution.com/' // pass the redirect url we put it in the instagram valid redirect url when generating client id
                        scopes={['public_content', 'follower_list']}
                        onLoginSuccess={(code) => {alert("Success Token :- " + code)}}
                        onLoginFailure={(data) => {alert("Failure :- "+data)}}
            />
     
      </View>
    );
  }

 async facebookLogin() {
        try {
          const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
      
          if (result.isCancelled) {
            // handle this however suites the flow of your app
            throw new Error('User cancelled request'); 
          }
      
          console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
      
          // get the access token
          const data = await AccessToken.getCurrentAccessToken();
      
          if (!data) {
            // handle this however suites the flow of your app
            throw new Error('Something went wrong obtaining the users access token');
          }
      
          // create a new firebase credential with the token
          const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
      
          // login with credential
          const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
          
          alert(JSON.stringify(firebaseUserCredential.user.toJSON()))
        } catch (e) {
          alert(e);
        }
      }

        // sign in button
    onSignIn()
    {
     
        // if(!validatePassword(this.state.user_password)){
        //     ToastAndroid.show('Password must be greater than 6 characters',ToastAndroid.LONG);
        // }
        // else{
        //     this.setState({loader:true});
        //     setTimeout(()=>{
        //         this.setState({loader:false})
        //     },5000);
        // }
        this.setState({loader:true})
        setTimeout(()=>{
            this.setState({loader:false},()=>this.props.navigation.navigate('TabNavigation'))
        },3000)
    }

}

export default Login;
