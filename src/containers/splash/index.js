import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Styles from '../common/style'

const SPLASH_TIME = 1000;
class SplashScreen extends Component {
  static navigationOptions = {
    header:null
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount(){
    setTimeout(()=>{
        this.props.navigation.navigate('SplashScreen2');
    },SPLASH_TIME)
  }

  render() {
    return (
      <View style={[Styles.alignCenter,Styles.justifyCenter,{flex:1}]}>
        <Text style={[Styles.fontSize_4]}> DESIGNED BY COLE </Text>
      </View>
    );
  }
}

export default SplashScreen;
