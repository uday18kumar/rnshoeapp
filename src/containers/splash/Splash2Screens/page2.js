import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Styles from '../../common/style'
import {responsiveHeight} from 'react-native-responsive-dimensions'

class Page2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={[{backgroundColor:'cornflowerblue',
                        flex:1
                        },
                        Styles.alignCenter]}>
                <View style={[{marginTop:responsiveHeight(60)},
                                Styles.width_80,
                                Styles.alignCenter,
                                Styles.textCenter]}>
                    <Text style={[Styles.fontSize_4,
                                  Styles.textCenter,
                                  Styles.fontBold]}>WELCOME</Text>
                    <Text style={[Styles.fontSize_1_5,
                                  Styles.textCenter]}>
                                    Lorem Ipsum is simply dummy text of the printing
                                    and typesetting industry. Lorem Ipsum has been the industry's
                                    standard dummy text ever since the 1500s, when an unknown printer
                                    took a galley of type and scrambled it to make a type specimen book.
                                    It has survived not only five centuries, but also the leap into electronic typesetting, 
                                    remaining essentially unchanged.
                    </Text>
                </View>
        </View>
    );
  }
}

export default Page2;
