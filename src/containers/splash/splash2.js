import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';
import { IndicatorViewPager, PagerDotIndicator} from 'rn-viewpager';
import Page1 from './Splash2Screens/page1'
import Page2 from './Splash2Screens/page2'
import Page3 from './Splash2Screens/page3'
import Styles from '../common/style'
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';


class Splash2 extends Component {
  static navigationOptions = {
    header:null
  };
  
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }

  render() {
    return (
        <View style={{flex:1}}>
        <IndicatorViewPager
            style={{height:responsiveHeight(97),
                    width:responsiveWidth(100),
                    position:'absolute'}}
            indicator={this._renderDotIndicator()}
        >
            <Page1 /> 
            <Page2 />
            <Page3 />
        </IndicatorViewPager>
        
        <View style={{alignItems:'flex-end',
                        marginTop: responsiveHeight(92),
                        marginRight:responsiveWidth(2)}}>    
            
            {/* skip button */}
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
                    <Text style={[Styles.fontSize_1_5,{color:'white'}]}>SKIP</Text>
            </TouchableOpacity>
            
        </View>
    </View>
    );
  }

//   _renderDotIndicator = (pages) => {
    
//         let tabs = [];
//         for(var i=0;i<pages;i++)
//         {
//             if(i == 0){
//                 tabs.push({
//                     iconSource: require('../../../assets/images/home-white.png'),
//                     selectedIconSource: require('../../../assets/images/home.png')
//                 });
//             }
//             else{
//                 tabs.push({
//                     iconSource: require('../../../assets/images/dot-white.png'),
//                     selectedIconSource: require('../../../assets/images/dot-yellow.png')
//                 });
//             }
//         }
//         return ( 
//             <PagerTabIndicator tabs={tabs} 
//                 style={{backgroundColor:'transparent',
//                 width:responsiveWidth(pages*4),
//                 height:responsiveHeight(2),
//                 marginBottom:responsiveHeight(2),
//                 marginLeft:responsiveWidth(40),
//                 borderTopColor:'transparent',
//                 alignItems:'center',
//                 justifyContent:'center',
//                 border:0}}/>
//         );
                    
//         }
    
     _renderDotIndicator(){
        return (<PagerDotIndicator dotStyle={'circle'} pageCount={3} /> );
    }
}

export default Splash2;
